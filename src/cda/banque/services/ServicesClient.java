package cda.banque.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

import cda.banque.control.ControlsClient;
import cda.banque.control.ControlsGeneral;
import cda.banque.entite.Administrateur;
import cda.banque.entite.Agence;
import cda.banque.entite.Client;
import cda.banque.entite.Compte;
import cda.banque.entite.Conseille;

public class ServicesClient {

	/**
	 * fonction qui crée et retourne une un client
	 * 
	 * @return un client
	 */
	public Client creeClient(String nom, String prenom, LocalDate dateNaissance, String email) {
		return new Client(nom, prenom, dateNaissance, email);

	}

	/**
	 * Fonction qui retourne le nombre de compte actif
	 * 
	 * @param compte : le tableau de liste de compte
	 * @return le nombre de compte actif
	 */
	public static int nbCompteactiv(ArrayList<Compte> compte) {
		int nbCompte = 0;
		for (int i = 0; i < compte.size(); i++) {
			if (compte.get(i).isActive() == true) {
				nbCompte++;
			}
		}
		return nbCompte;
	}

//	/**
//	 * Fonction qui permet d'activer un comte client
//	 * 
//	 * @param client le client a activer
//	 * @return un boolean qui permet de verifier que le client est bien active
//	 */
//	public static boolean activerClient(Client client) {
//		if (client.isActive() != true) {
//			client.setActive(true);
//			return true;
//		} else {
//			System.out.println("Le client est deja active !");
//		}
//		return false;
//	}

	public static String numClient(char d, char f, int incremente) {
		String idClient = null;
		String inc = "" + incremente;
		while (inc.length() < 6) {
			inc = '0' + inc;
		}
		idClient = "" + d + f + inc;
		return idClient;
	}

//	/**
//	 * Fonction qui permet de desactiver un comte client
//	 * 
//	 * @param client le client a desactiver
//	 * @return un boolean qui permet de verifier que le client est bien active
//	 */
//	public static boolean desactiverClient(Client client) {
//		if (client.isActive() != false) {
//			client.setActive(false);
//			return true;
//		} else {
//			System.out.println("Le client est deja desactive !");
//		}
//		return false;
//	}

//	/**
//	 * Fonction qui permet choisir le parametre que l'on veux changer
//	 * @param client pour lequel on veut changer ses information
//	 */
//	public static boolean modifInfoClient(Client client,Scanner in) {
//		IhmClient.afficherModif();
//			boolean mauvaisChoix= false;
//		do {
//			char choix = in.nextLine().charAt(0);
//				switch (choix) {	
//				case '1' :
//					client.setNom(nomClient(in));
//					return true;
//				case '2' :
//					client.setPrenom(prenomClient(in));
//					return true;	
//				case '3' :
//					client.setDateDeNaissance(dateNaissanceClient(in));
//					return true;
//				case '4' :
//					client.setEMail(eMailClient(in));
//					return true;
//				default: System.out.println("Votre choix n'est pas correct !");
//					mauvaisChoix = true;
//				}
//			}while(mauvaisChoix);
//		return false;
//		}

	/**
	 * Fonction qui retourne un compte en fonction du numero de compte
	 * 
	 * @param client    pour lequel on recherche un compte
	 * @param numCompte le numero de compte rechercher
	 * @return le compte recherche
	 */
	public static Compte rechercheCompte(Client client, String numCompte) {
		for (Compte compte : client.getCompte()) {
			if (compte != null && numCompte.equals(compte.getNumero()))
				return compte;
		}
		return null;
	}

//	/**
//	 * alimente un compte
//	 * 
//	 * @param client	le client
//	 * @param numCompte	le numero du compte
//	 * @param montant	le scanner
//	 * @return	true si l'alimentation a ete effectuer, false si non
//	 */
//	public static boolean alimenterUnCompte(Administrateur admin, Client client ,  String numCompte, double montant) {
//		if(montant >= 0) {
//			for (Compte compte : client.getCompte()) {
//				if(compte.isActive() && compte.getNumero().equals(numCompte)) {
//					try {
//						compte.setSolde(compte.getSolde()+montant);
//						Services.creationTransaction(admin, 'A', numCompte, montant);
//						return true;
//					} catch (IOException e) {
//						System.out.println("generation de transaction echouer");
//						return false;
//					}
//				}
//			}return false;
//		} else {
//		System.out.println("le montant est invalide");
//		}
//		return false;
//	}

//	/**
//	 * Fait un retrait du compte
//	 * 
//	 * @param client	le client
//	 * @param numCompte	le numero du compte
//	 * @param montant	le montant a retirer
//	 * @return	true si le retrait a ete effectuer, false si non
//	 */
//	public static boolean retirerArgent(Administrateur admin, Client client, String numCompte, double montant) {
//		if(montant > 0) {
//			for (Compte compte : client.getCompte()) {
//				if(compte.isActive() &&  compte.getNumero().equals(numCompte)) {
//					if(compte.isDecouvert()) {
//						compte.setSolde(compte.getSolde()-montant);
//						try {
//							Services.creationTransaction(admin, 'R', numCompte, montant);
//						} catch (IOException e) {
//							System.out.println("generation de transaction echouer");
//							return false;
//						}
//						return true;
//					}
//					else {
//						if(compte.getSolde() < montant)
//							return false;
//						try {
//							Services.creationTransaction(admin, 'A', numCompte, montant);
//						} catch (IOException e) {
//							System.out.println("generation de transaction echouer");
//							return false;
//						}
//						compte.setSolde(compte.getSolde()-montant);
//						return true;
//					}
//				}
//			}
//		}
//		return false;
//	}
	public static Client rechercheClientViaLogin(Administrateur admin, String login) {
		for (Agence tourisque : admin.getAgence()) {
			for (Conseille agricole : tourisque.getConseille()) {
				for (Client pigeon : agricole.getClient()) {
					if (login.split("~")[0].equals(pigeon.getLogin()))
						return pigeon;
				}
			}
		}
		return null;
	}

}
