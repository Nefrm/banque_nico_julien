package cda.banque.services;

import java.util.Scanner;

import cda.banque.entite.Client;
import cda.banque.entite.Compte;
import cda.banque.entite.Courant;
import cda.banque.entite.LivretA;
import cda.banque.entite.PEL;

public class ServiceCompte {
	//M�thode(s)
	/**
	 * Active un compte
	 * 
	 * @param client	Le client
	 * @param numCompte	le numero du compte a activer
	 * @return	true si le compte a �t� activ�, false si non
	 */
	public boolean activerUnCompte(Client client, String numCompte) {
		for (Compte compte : client.getCompte()) {
			if(compte.getNumero().equals(numCompte)) {
				if(compte.isActive()) {
					System.out.println("le compte est d�j� activ�");
					return false;
				}
				else if(ServicesClient.nbCompteactiv(client.getCompte()) == 3) {
					System.out.println("le compte a d�j� 3 compte actifs");
					return false;
				}
				else {
					compte.setActive(true);
					System.out.println("le compte a �t� activ�");
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * D�sactive un compte
	 * 
	 * @param client	le client
	 * @param numCompte	le compte a d�sactiver
	 * @return	true si le compte a �t� d�sactiv�, false si non
	 */
	public boolean desactiverUnCompte(Client client, String numCompte) {
		for (Compte compte : client.getCompte()) {
			if(compte.getNumero().equals(numCompte)) {
				if(!compte.isActive()) {
					System.out.println("le compte est d�j� d�sactiv�");
					return false;
				}else if ("Compte".equals(compte.getClass().getSimpleName())) {
					System.out.println("le compte ne peut pas etre d�sactiv�");
					return false;
				}else {
					compte.setActive(false);
					System.out.println("le compte a �t� d�sactiv�");
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * calcul les frais du compte
	 * 
	 * @param compte	le compte a calculer
	 * @return	les frais calcul�s
	 */
	public double CalculerFrais(Compte compte) {
		if(compte instanceof PEL)
			return compte.getFrais()+compte.getSolde()*0.025;
		else if(compte instanceof LivretA)
			return compte.getFrais()+compte.getSolde()*0.1;
		else
			return compte.getFrais();
	}
	
	/**
	 * cr�er un nouveau compte
	 * 
	 * @return	un nouveau compte
	 */
	public Compte creerCompte(Scanner sc) {
		char reponse = ' ';
		while(reponse != 'o' && reponse != 'n') {
			System.out.println("autoriser le d�couvert ? o/n");
			reponse = sc.nextLine().toLowerCase().charAt(0);
		}
		
		if(reponse == 'o')
			return new Courant(true);
		else
			return new Courant(false);
	}
	/**
	 * Fonction qui demande le choix du compte que l'on veut creer
	 * @param client	le client our qui on veux creer le compte
	 * @param in
	 * @return	le type de compte que l'on a choisie
	 */
	public Compte choixCompte(Client client , Scanner in) {
		if( client.getCompte().size() == 0) {
			return creerCompte(in);
		}else {
		System.out.println("Quelle type de compte voulez vous creer ?\n1 - Compte courant\n2 - Compte Livret A\n3 - Compte PEL");
		String choix = in.nextLine();
		if("1".equals(choix)) {
			return creerCompte(in);
		}
		if("2".equals(choix)) {
			return new ServiceLivretA().creerLivretA(in);
		}
		if("3".equals(choix)) {
			return new ServicePEL().creerPEL(in);
		}
		return null;
		}
	}
}
