package cda.banque.services;

import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

import cda.banque.control.ControlsAgence;
import cda.banque.entite.Administrateur;
import cda.banque.entite.Agence;
import cda.banque.entite.Client;
import cda.banque.entite.Compte;
import cda.banque.entite.Conseille;

public class ServiceAgence {
	// M�thode(s)
	/**
	 * Cr�er une agence
	 * 
	 * @return la nouvelle agence
	 */
	public Agence creerAgence(Scanner sc) {
		String nomA = null;
		String adresseA = null;
		do {
			System.out.println("veuillez entrer le nom de l'agence :");
			nomA = sc.nextLine();
		} while (!ControlsAgence.validNomAgence(nomA));
		do {
			System.out.println("veuillez entrer l'adresse de l'agence :");
			adresseA = sc.nextLine();
		} while (!ControlsAgence.validerAddresseAgence(adresseA));
		return new Agence(nomA, adresseA);
	}

//	/**
//	 * Change la domiciliation d'un client vers une autre agence
//	 * 
//	 * @param agence1    l'agence actuelle du client
//	 * @param conseille1 le conseill� actuel du client
//	 * @param client     le client
//	 * @param agence2    la nouvelle agence du client
//	 * @param conseille2 le nouveau conseill� du client
//	 * @return true si la domiciliation c'est bien pass�, false si non
//	 */
//	public boolean changerDomiciliation(Agence agence1, Conseille conseille1, Client client, Agence agence2,
//			Conseille conseille2) {
//		if (agence1 == null) {
//			System.out.println("la premiere agence n'existe pas");
//			return false;
//		} else if (client == null) {
//			System.out.println("le client n'existe pas");
//			return false;
//		} else if (agence2 == null) {
//			System.out.println("la seconde agence n'existe pas");
//			return false;
//		} else if (agence1 == agence2) { // Si les agences sont la meme instance
//			System.out.println("l'agence ne doit pas etre la meme");
//			return false;
//		}
//		int indexOfConseille1 = -1;
//		int indexOfConseille2 = -1;
//		Iterator<Conseille> itCons1 = agence1.getConseille().iterator();
//		Iterator<Conseille> itCons2 = agence2.getConseille().iterator();
//		// Recherche de l'indice du conseill� de la premiere agence
//		boolean find = false;
//		while (!find) {
//			if (!itCons1.hasNext()) {
//				indexOfConseille1 = -1;
//				break;
//			}
//			if (itCons1.next().equals(conseille1)) {
//				find = true;
//			} else
//				indexOfConseille1++;
//		}
//
//		// Recherche de l'indice du conseill� de la seconde agence
//		find = false;
//		while (!find) {
//			if (!itCons2.hasNext())	{
//				indexOfConseille2 = -1;
//				break;
//			}
//			if (itCons2.next().equals(conseille2)) {
//				find = true;
//			} else
//				indexOfConseille2++;
//		}
//		Iterator<Client> itClient;
//		if (indexOfConseille1 >= 0)
//			itClient = agence1.getConseille().get(indexOfConseille1).getClient().iterator();
//		else
//			return false;
//		// Recherche de l'indice du client du conseill�
//		int indexOfClient = -1;
//		find = false;
//		while (!find) {
//			if (itClient.equals(client)) {
//				find = true;
//			} else
//				indexOfClient++;
//			if (itClient.hasNext())
//				itClient.next();
//			else {
//				indexOfClient = -1;
//				find = true;
//			}
//		}
//
//		if (agence1.getConseille().get(indexOfConseille1) == null) { // si le conseill� n'est pas dans l'agence 1
//			System.out.println("le conseill� de la premiere agence n'existe pas");
//			return false;
//		} else if (agence2.getConseille().get(indexOfConseille2) == null) { // si le conseill� n'est pas dans l'agence 2
//			System.out.println("le conseill� de la seconde agence n'existe pas");
//			return false;
//		} else if (indexOfClient == -1
//				|| agence1.getConseille().get(indexOfConseille1).getClient().get(indexOfClient) == null) { // si le client
//																										// n'existe pas
//																										// dans l'agence
//			System.out.println("le client n'existe pas dans l'agence");
//			return false;
//		}
//		agence2.getConseille().get(indexOfConseille2).getClient()
//				.add(agence1.getConseille().get(indexOfConseille1).getClient().get(indexOfClient));
//		agence1.getConseille().get(indexOfConseille1).getClient().remove(indexOfClient);
//		try {
//			Services.domiciliationClient(agence1, agence1, client);
//		} catch (IOException e) {
//
//		}
//		return true;
//	}

	/**
	 * Recherche d'un conseille
	 * 
	 * @param agence	l'agence
	 * @param login	le login du conseille
	 * @return	le conseille s'il existe, null si non
	 */
	public static Conseille rechercheConseille(Agence agence, String login) {
		for (Conseille conseille : agence.getConseille()) {
			if (conseille.getLogin().equals(login)) {
				return conseille;
			}
		}
		return null;
	}

	/**
	 * Recherche le client
	 * 
	 * @param agence	l'agence
	 * @param numClient	le numero du client
	 * @return	le client s'il existe, null si non
	 */
	public static Client rechercheClient(Agence agence, String numClient) {
		for (Conseille conseille : agence.getConseille()) {
			Client client = ServiceConseille.rechercheClient(conseille , numClient );
			if (client != null)
				return client;
		}
		return null;
	}

	/**
	 * Recherche de compte
	 * 
	 * @param agence	l'agence
	 * @param numCompte	le numero du compte
	 * @return	le compte s'il existe, null si non
	 */
	public static Compte rechercheCompte(Agence agence, String numCompte) {
		for (Conseille conseille : agence.getConseille()) {
			Compte compte = ServiceConseille.rechercheCompte(conseille , numCompte);
			if (compte != null)
				return compte;
		}
		return null;
	}
	
//	/**
//	 * alimente un compte
//	 * 
//	 * @param agence	l'agence
//	 * @param numCompte	le numero du compte
//	 * @param montant	le scanner
//	 * @return	true si l'alimentation est effectuer, false si non
//	 */
//	public static boolean alimenterUnCompte(Administrateur admin, Agence agence, String numCompte, double montant) {
//		for (Conseille conseille : agence.getConseille()) {
//			if(ServiceConseille.alimenterUnCompte(admin, conseille, numCompte, montant))
//				return true;
//		}
//		return false;
//	}

//	/**
//	 * Retire de l'argent d'un compte
//	 * 
//	 * @param agence	l'agence
//	 * @param numCompte	le numero de compte
//	 * @param montant	le scanner
//	 * @return	true si le retrait a ete effectuer, false si non
//	 */
//	public static boolean retirerArgent(Administrateur admin, Agence agence, String numCompte, double montant) {
//		for (Conseille conseille : agence.getConseille()) {
//			if(ServiceConseille.retirerArgent(admin, conseille, numCompte, montant))
//				return true;
//		}
//		return false;
//	}

//	public static boolean virement(Administrateur admin, Agence agence, String numCompteSource, String numCompteDest, Scanner in) {
//		System.out.println("choississez le montant :");
//		double montant = in.nextDouble();
//		in.nextLine();
//		if(retirerArgent(admin, agence, numCompteSource, montant) && alimenterUnCompte(admin, agence, numCompteDest, montant)) {
//			try {
//				Services.creationTransaction(admin, 'V', numCompteSource, montant);
//				Services.creationTransaction(admin, 'V', numCompteDest, montant);
//			} catch (IOException e) {
//				System.out.println("un probleme est survenu lors de la transactions.");
//				return false;
//			}
//			return true;
//		}
//		return false;
//	}
}
