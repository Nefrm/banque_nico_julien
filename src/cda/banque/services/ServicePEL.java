package cda.banque.services;

import java.util.Scanner;

import cda.banque.entite.PEL;

public class ServicePEL {
	//M�thode(s)
	/**
	 * Cr�er un nouveau compte de type PEL
	 * 
	 * @return	le nouveau compte PEL
	 */
	public PEL creerPEL(Scanner sc) {
		char reponse = ' ';
		while(reponse != 'o' && reponse != 'n') {
			System.out.println("autoriser le d�couvert ? o/n");
			reponse = sc.nextLine().toLowerCase().charAt(0);
		}
		if(reponse == 'o')
			return new PEL(true);
		else
			return new PEL(false);
	}
}
