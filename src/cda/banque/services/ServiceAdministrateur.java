package cda.banque.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import cda.banque.entite.Administrateur;
import cda.banque.entite.Agence;
import cda.banque.entite.Client;
import cda.banque.entite.Compte;
import cda.banque.entite.Conseille;

public class ServiceAdministrateur {
	//Attributs
	private int logIncrementAdmin = 0;
	
	//M�thodes
//	/**
//	 * Permet de cr�er un administrateur
//	 * 
//	 * @param sc	le scanner pour demander les infos de la cr�ation
//	 * @return	un nouvel administrateur
//	 */
//	public Administrateur creerAdministrateur(Scanner sc) {
//		String log = "ADM";
//		if(logIncrementAdmin >= 100) {
//			System.out.println("le nombre d'admin maximum a �t� atteint");
//			return null;
//		}else {
//			String str = "";
//			str += ++logIncrementAdmin;
//			while(str.length() < 2)
//				str = "0"+str;
//			log += str;
//		}
//		Conseille conseille = new ServiceConseille().creerConseille(sc);
//		Administrateur admin = new Administrateur(conseille.getNom(), conseille.getPrenom(), conseille.getDateDeNaissance(), conseille.getEMail(), log);
//		try {
//			Services.ajoutListeAdmin(log, log);
//		} catch (IOException e) {
//			try {
//				Services.creationDossier("ressource");
//				BufferedWriter bw = new BufferedWriter(new FileWriter("ressource\\admin.txt",false));
//				bw.write(admin.getLogin()+"~"+Services.encodage(admin.getLogin()));
//				bw.newLine();
//				bw.close();
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//		}
//		return admin;
//	}
	
	/**
	 * Recherche d'une agence
	 * 
	 * @param administrateur	l'administrateur
	 * @param codeAgence	le code de l'agence
	 * @return	l'agence rechercher, null si non
	 */
	public static Agence rechercheAgence(Administrateur administrateur , String codeAgence) {
		for (Agence agence : administrateur.getAgence()) {
			if(agence.getCodeAgence().equals(codeAgence)) {
				return agence;
			}
		}
		return null;
	}
	
	/**
	 * Recherche d'un conseille
	 * 
	 * @param administrateur	l'administrateur
	 * @param login	le login du conseille
	 * @return	le conseille s'il existe, null si non
	 */
	public static Conseille rechercheConseille(Administrateur administrateur , String login) {
		for (Agence agence : administrateur.getAgence()) {
			return ServiceAgence.rechercheConseille(agence, login);
		}
		return null;
	}
	
	/**
	 * Recherche un client
	 * 
	 * @param administrateur	l'administrateur
	 * @param numClient	le numero du client
	 * @return	le client s'il existe, null si non
	 */
	public static Client rechercheClient(Administrateur administrateur , String numClient) {
		for (Agence agence : administrateur.getAgence()) {
			Client client = ServiceAgence.rechercheClient(agence , numClient);
			if (client != null)
				return client;
		}
		return null;
	}
	
	/**
	 * Recherche un compte
	 * 
	 * @param administrateur	l'administrateur
	 * @param numCompte	le numero du compte
	 * @return	le compte s'il existe, null si non
	 */
	public static Compte rechercheCompte(Administrateur administrateur , String numCompte) {
		for (Agence agence : administrateur.getAgence()) {
			Compte compte = ServiceAgence.rechercheCompte(agence ,  numCompte);
			if (compte != null)
				return compte;
		}
		return null;
	}
	
//	/**
//	 * alimenter un compte
//	 * 
//	 * @param admin	l'admin qui cherche
//	 * @param numCompte	le numero du compte a alimenter
//	 * @param montant	le scanner
//	 * @return	true si l'alimentation a ete effectuer
//	 */
//	public static boolean alimenterUnCompte(Administrateur admin, String numCompte, double montant) {
//		for (Agence agence : admin.getAgence())
//			if(ServiceAgence.alimenterUnCompte(admin, agence, numCompte, montant))
//				return true;
//		return false;
//	}
	
//	/**
//	 * Retire de l'argent d'un compte
//	 * 
//	 * @param admin	l'admin
//	 * @param numCompte	le numero du compte
//	 * @param in	le scanner
//	 * @return	true si l'argent a ete retirer, false si non
//	 */
//	public static boolean retirerArgent(Administrateur admin, String numCompte, double montant) {
//		for (Agence agence : admin.getAgence()) {
//			if(ServiceAgence.retirerArgent(admin, agence, numCompte, montant))
//				return true;
//		}
//		return false;
//	}
	
//	/**
//	 * fais un virement
//	 * 
//	 * @param admin	l'administrateur
//	 * @param numCompteSource	le numero du compte source
//	 * @param numCompteDest	le numero du compte de destination
//	 * @param in	le scanner
//	 * @return	true si le virement a ete effectuer, false si non
//	 */
//	public static boolean virement(Administrateur admin, String numCompteSource, String numCompteDest, Scanner in) {
//		System.out.println("choississez le montant :");
//		double montant = in.nextDouble();
//		in.nextLine();
//		if(retirerArgent(admin, numCompteSource, montant) && alimenterUnCompte(admin, numCompteDest, montant)) {
//			try {
//				Services.creationTransaction(admin, 'V', numCompteSource, montant);
//				Services.creationTransaction(admin, 'V', numCompteDest, montant);
//			} catch (IOException e) {
//				System.out.println("un probleme est survenu lors de la transactions.");
//				return false;
//			}
//			return true;
//		}
//		return false;
//	}
}
