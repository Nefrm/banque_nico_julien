package cda.banque.services;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

import javax.swing.JTable;

import cda.banque.DTO.DTO;
import cda.banque.control.ControlsGeneral;
import cda.banque.entite.Administrateur;
import cda.banque.entite.Agence;
import cda.banque.entite.Client;
import cda.banque.entite.Compte;
import cda.banque.entite.Conseille;
import cda.banque.servicesbdd.ServiceBDD;

public class Services {

	// /**
	// * Ajoute un conseille a la liste de conseille
	// *
	// * @param agence l'agence du nouveau conseille
	// * @param conseille le conseille
	// * @param mdp le mot de passe du conseille
	// * @return true si l'ajout a ete effectue, false si non
	// * @throws IOException
	// */
	// public static boolean ajoutListeConseille(Agence agence, Conseille conseille,
	// String mdp) throws IOException {
	// BufferedReader brco = new BufferedReader(new
	// FileReader("ressource\\"+agence.getCodeAgence()+"\\conseille\\listeConseille.txt"));
	// boolean exist = false;
	// while(brco.ready()) {
	// String log[] = brco.readLine().split("~");
	// if(log[0].equals(conseille.getLogin()) && log[0].equals(encodage(mdp))) {
	// exist = true;
	// }
	// }
	// brco.close();
	// if(!exist) {
	// BufferedWriter bwco = new BufferedWriter(new
	// FileWriter("ressource\\"+agence.getCodeAgence()+"\\conseille\\listeConseille.txt",
	// true));
	// bwco.write(conseille.getLogin()+"~"+encodage(mdp));bwco.newLine();
	// bwco.close();
	// return true;
	// }
	// return false;
	// }

	// /**
	// * Ajoute a la liste des clients
	// *
	// * @param agence l'agence du client
	// * @param client le client
	// * @param mdp le mot de passe du client
	// * @return true si le client a ete ajouter, false si non
	// * @throws IOException
	// */
	// public static boolean ajoutListeClient(Agence agence, Client client, String
	// mdp) throws IOException {
	// BufferedReader brco = new BufferedReader(new
	// FileReader("ressource\\"+agence.getCodeAgence()+"\\clients\\listeClients.txt"));
	// boolean exist = false;
	// while(brco.ready()) {
	// String log[] = brco.readLine().split("~");
	// if(log[0].equals(client.getLogin()) && log[0].equals(encodage(mdp))) {
	// exist = true;
	// }
	// }
	// brco.close();
	// if(!exist) {
	// BufferedWriter bwco = new BufferedWriter(new
	// FileWriter("ressource\\"+agence.getCodeAgence()+"\\clients\\listeClients.txt",
	// true));
	// bwco.write(client.getLogin()+"~"+encodage(mdp));bwco.newLine();
	// bwco.close();
	// return true;
	// }
	// return false;
	// }

	// /**
	// * Change le repertoire du client vers le nouveau
	// *
	// * @param agenceSource l'agence du client
	// * @param agenceDest l'agence de destination
	// * @param client le client a domicilier
	// * @return true si le client a ete domicilier, false si non
	// * @throws IOException
	// */
	// public static boolean domiciliationClient(Agence agenceSource, Agence
	// agenceDest, Client client) throws IOException {
	// //cr�er le dossier dans l'agence de destination
	// dossierClient(agenceDest, client);
	//
	// //lecture des log de l'agence source et copie des log du client vers les logs
	// de l'agence de destination
	// BufferedReader brcl = new BufferedReader(new
	// FileReader("ressource\\"+agenceSource.getCodeAgence()+"\\clients\\listeClients.txt"));
	// //Cr�ation d'un nouveau fichier de liste de client
	// BufferedWriter bwcls = new BufferedWriter(new
	// FileWriter("ressource\\"+agenceSource.getCodeAgence()+"\\clients\\listeClients"));
	// //ouverture du fichier de destination
	// BufferedWriter bwcl = new BufferedWriter(new
	// FileWriter("ressource\\"+agenceDest.getCodeAgence()+"\\clients\\listeClients.txt",true));
	// while(brcl.ready()) {
	// String[] log = brcl.readLine().split("~");
	// if(!log[0].equals(client.getLogin())) {
	// bwcls.write(log[0] + "~" + log[1]); //ajouter au nouveau fichier source
	// bwcls.newLine();
	// }
	// }
	// bwcls.close();
	// bwcl.close();
	// brcl.close();
	//
	// //Supprimer l'ancien fichier
	// new
	// File("ressource\\"+agenceSource.getCodeAgence()+"\\clients\\listeClients.txt").delete();
	//
	// //Renommer le fichier
	// new
	// File("ressource\\"+agenceSource.getCodeAgence()+"\\clients\\listeClients").renameTo(new
	// File("ressource\\"+agenceSource.getCodeAgence()+"\\clients\\listeClients.txt"));
	//
	// //Copie de toutes les transactions
	//
	// BufferedReader brts = new BufferedReader(new
	// FileReader("ressource\\"+agenceSource.getCodeAgence()+"\\clients\\"+client.getNumClient()+"\\transactions.txt"));
	// BufferedWriter bwtd = new BufferedWriter(new
	// FileWriter("ressource\\"+agenceDest.getCodeAgence()+"\\clients\\"+client.getNumClient()+"\\transactions.txt"));
	// while(brts.ready()) {
	// bwtd.write(brts.readLine());
	// }
	// brts.close();
	// bwtd.close();
	// new
	// File("ressource\\"+agenceSource.getCodeAgence()+"\\clients\\"+client.getNumClient()+"\\transactions.txt").delete();
	// new
	// File("ressource\\"+agenceSource.getCodeAgence()+"\\clients\\"+client.getNumClient()+"\\comptes.txt").delete();
	// return new
	// File("ressource\\"+agenceSource.getCodeAgence()+"\\clients\\"+client.getNumClient()).delete();
	// }

	/**
	 * Permet de se logger avec un log
	 * 
	 * @param admin l'admin
	 * @param log   les logs
	 * @return true si le log est correct, false si non
	 */
	public static boolean login(String login, String mdp) {
		return DTO.login(login, mdp);
	}
	
	public static String[] getAgence() {
		return DTO.getAgence();
	}
	
	public static String[] getConseille() {
		return DTO.getConseille();
	}

	public static boolean ajoutClientBDD(String nom, String prenom, String naissance, String agence, String conseille, String email, JTable table) {
		return DTO.ajoutClientBDD(nom, prenom, naissance, agence, conseille, email, table);
	}
	
	public static boolean ajoutCompteBDD(int type, float solde, boolean decouvert) {
		return false;
		
	}
	/**
	 * Fonction qui permet de changer une saisie en date
	 * 
	 * @param str la saisie de la date
	 * @return une localDate
	 */
	public static LocalDate date(String str) {
		String[] strDate = str.split("/");
		LocalDate date = null;
		for (int i = 0; i < strDate.length; i++) {
			try {
				date = LocalDate.parse(strDate[2] + "-" + strDate[1] + "-" + strDate[0]);
			} catch (DateTimeParseException e) {
			}
		}
		return date;
	}

	/**
	 * Fonction qui permet de changer une saisie en date
	 * 
	 * @param str la saisie de la date
	 * @return une localDate
	 */
	public static LocalDate date2(String str) {
		String[] strDate = str.split("-");
		LocalDate date = null;
		for (int i = 0; i < strDate.length; i++) {
			try {
				date = LocalDate.parse(strDate[2] + "-" + strDate[1] + "-" + strDate[0]);
			} catch (DateTimeParseException e) {
			}
		}
		return date;
	}

	/**
	 * permet d'encoder un mot avec le code de cesar
	 * 
	 * @param str la chaine a coder
	 * @return la chaine code
	 */
	public static String encodage(String str) {
		String code = "";
		for (int i = 0; i < str.length(); i++) {
			code += (char) (str.charAt(i) + 3);
		}
		return code;
	}

	/**
	 * permet de decoder un mot avec le code de cesar
	 * 
	 * @param str le mot a decoder
	 * @return le mot decode
	 */
	public static String decodage(String str) {
		String code = "";
		for (int i = 0; i < str.length(); i++) {
			code += (char) (str.charAt(i) - 3);
		}
		return code;
	}

//	/**
//	 * Ajoute l'admin dans la liste
//	 * 
//	 * @param login le login du nouvel admin
//	 * @param mdp   le mdp du nouvel admin
//	 * @return true si l'admin a ete ajouter, false si non
//	 * @throws IOException
//	 */
//	public static boolean ajoutListeAdmin(String login, String mdp) {
//		// Charger et configurer le driver de la base de données.
//		String driverName = "org.postgresql.Driver";
//		try {
//			Class.forName(driverName);
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		// Infos de connection
//		props.setProperty("user", "postgres");
//		props.setProperty("password", "admin");
//		// props.setProperty("ssl","true");
//
//		try {
//			conn = DriverManager.getConnection(url, props);
//			stmt = conn.createStatement();
//
//			ResultSet resultats = null;
//			// Requette à executer
//			String requete = "INSERT INTO personne VALUES (0,1,\'\',\'\')";
//			try {
//				// La fonction executeQuery permet d'executer la requêtte
//				stmt.executeUpdate(requete);
//			} catch (SQLException e) {
//
//			}
//
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//			if (conn != null)
//				try {
//					conn.close();
//				} catch (SQLException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//		}
//
////		BufferedReader br = new BufferedReader(new FileReader("ressource\\admin.txt"));
////		while(br.ready()) {
////			String[] log = br.readLine().split("~");
////			if(log[0].equals(login) && decodage(log[1]).equals(mdp)) {
////				br.close();
////				return false;
////			}
////		}
////		br.close();
////		BufferedWriter bw = new BufferedWriter(new FileWriter("ressource\\admin.txt",true));
////		bw.write(login+"~"+encodage(mdp));bw.newLine();
////		bw.close();
//		return true;
//	}

//	/**
//	 * Creer un fichier pour repertorier les compte du client
//	 * 
//	 * @param agence	l'agence
//	 * @param client	le client
//	 * @return	true si le compte a ete ajouter, false si non
//	 * @throws IOException
//	 */
//	public static boolean ajouterCompte(Agence agence, Client client) {
//		try {
//			for (Compte compte : client.getCompte()) {
//				BufferedReader brco = new BufferedReader(new FileReader("ressource\\"+agence.getCodeAgence()+"\\clients\\"+client.getNumClient()+"\\comptes.txt"));
//				boolean exist = false;
//				while(brco.ready()) {
//					String[] log = brco.readLine().split("~");
//					if(log[0].equals(compte.getNumero())) {
//						exist = true;
//					}
//				}
//				brco.close();
//				if(!exist) {
//					BufferedWriter bwco = new BufferedWriter(new FileWriter("ressource\\"+agence.getCodeAgence()+"\\clients\\"+client.getNumClient()+"\\comptes.txt",true));
//					bwco.write(compte.getNumero());
//					bwco.write("~");
//					bwco.write(compte.getClass().getSimpleName());bwco.newLine();
//					bwco.close();
//				}
//			}
//			return true;
//		} catch (IOException e) {
//			return false;
//		}
//	}

//	/**
//	 * Creer les fichier de transaction
//	 * 
//	 * @param agence	l'agence du client qui fais la transaction
//	 * @param client	le client qui effectue la transactions
//	 * @param typeTransaction	le type de la transaction (V,R,A)
//	 * @param compte	le compte concerne
//	 * @param montant	le montant de la transaction susdite
//	 * @throws IOException 
//	 */
//	public static void creationTransaction(Administrateur admin, char typeTransaction, String numCompte, double montant) throws IOException {
//		Agence agence = rechercheAgenceViaNumCompte(admin, numCompte);
//		Client client = rechercheClientViaNumCompte(admin, numCompte);
//		Compte compte = ServiceAdministrateur.rechercheCompte(admin, numCompte);
//		BufferedWriter bwco = new BufferedWriter(new FileWriter("ressource\\"+agence.getCodeAgence()+"\\clients\\"+client.getNumClient()+"\\transactions.txt", true));
//		bwco.write(LocalDate.now().toString());
//		bwco.write("~");
//		bwco.write(typeTransaction);
//		bwco.write("~");
//		bwco.write(compte.getClass().getSimpleName());
//		bwco.write("~");
//		bwco.write(compte.getNumero());
//		bwco.write("~");
//		bwco.write(String.valueOf(montant));
//		bwco.write("~");
//		bwco.write(String.valueOf(compte.getSolde()));bwco.newLine();
//		bwco.close();
//	}

	/**
	 * Recherche d'un client via son numero de compte
	 * 
	 * @param agence    l'agence
	 * @param numCompte le numero de compte
	 * @return le client correspondant au compte, null si non
	 */
	private static Client rechercheClientViaNumCompte(Agence agence, String numCompte) {
		for (Conseille conseille : agence.getConseille())
			for (Client client : conseille.getClient())
				for (Compte compte : client.getCompte())
					if (compte.getNumero().equals(numCompte))
						return client;
		return null;
	}

	/**
	 * recherche d'un client via son numero de compte
	 * 
	 * @param admin     l'admin
	 * @param numCompte le numero de compte
	 * @return le client correspondant au compte, null si non
	 */
	private static Client rechercheClientViaNumCompte(Administrateur admin, String numCompte) {
		for (Agence agence : admin.getAgence())
			return rechercheClientViaNumCompte(agence, numCompte);
		return null;
	}

	/**
	 * recherche d'une agence via le numero de compte
	 * 
	 * @param admin     l'admin
	 * @param numCompte le numero du compte
	 * @return l'agence correspondante, null si non
	 */
	private static Agence rechercheAgenceViaNumCompte(Administrateur admin, String numCompte) {
		for (Agence agence : admin.getAgence())
			for (Conseille conseille : agence.getConseille())
				for (Client client : conseille.getClient())
					for (Compte compte : client.getCompte())
						if (compte.getNumero().equals(numCompte))
							return agence;
		return null;
	}

//	/**
//	 * Fonction qui retourne un tableau de tableau d'information de transaction
//	 * @param codeAgence	le code agence qui contient le client
//	 * @param numClient		le numero du client concerne
//	 * @param in	le scanner pour la saisie des date de la periode
//	 * @return	un tableau de transaction contenant des tableaux d'informations de transaction
//	 */
//	public ArrayList<String> transact(String codeAgence , String numClient, Scanner in){
//		System.out.println("Entrer la date de debut de la periode");
//		String dateDebut= in.nextLine();
//		System.out.println("Entrer la date de fin de la periode");
//		String dateFin= in.nextLine();
//		ArrayList<String>transact= new ArrayList<String>();
//		try {
//			FileReader fr = new FileReader(chemin(codeAgence, numClient));
//			BufferedReader br = new BufferedReader(fr);
//			while(br.ready()) {
//					String ligne = br.readLine();
//				if (ControlsGeneral.validTransacDate(dateDebut, dateFin, ligne))
//					transact.add(ligne);
//				}
//			fr.close();
//			br.close();
//		} catch (IOException e) {
//			System.out.println("Il n'y a pas de transaction !");
//		}
//
//		return transact;
//	}	

	/**
	 * recherche d'une agence via le numero de compte
	 * 
	 * @param admin     l'admin
	 * @param numCompte le numero du compte
	 * @return l'agence correspondante, null si non
	 */
	private static Agence rechercheAgenceViaNumClient(Administrateur admin, String numClient) {
		for (Agence agence : admin.getAgence())
			for (Conseille conseille : agence.getConseille())
				for (Client client : conseille.getClient())
					if (client.getNumClient().equals(numClient))
						return agence;
		return null;
	}

//	public void consulterOperation(Administrateur administrateur , Client client , Scanner in) {
//		String codeAgence = rechercheAgenceViaNumClient(administrateur, client.getNumClient()).getCodeAgence();
//		ArrayList<String>transaction = transact(codeAgence, client.getNumClient(), in);
//		System.out.println("                                  Operations                                                            ");
//		System.out.println("_______________________________________________________________________________________________");
//		System.out.println("Date de l'operation:   Operation:    Type de compte:    Numero de compte:    Montant:    Solde:");
//		for (String ligne : transaction) {
//			String[] sousLigne = ligne.split("~");
//				System.out.println(sousLigne[0]+"             "+operation(sousLigne[1])+"      "+typeCompte(sousLigne[2])+"     "+sousLigne[3]+"           "+sousLigne[4]+"        "+sousLigne[5]);
//		}
//	}
//	/**
//	 * Fonction qui retourne le type d'operation
//	 * 
//	 * @param typeOperation code du type d'operation
//	 * @return type d'operation
//	 */
//	public String operation(String typeOperation) {
//		String op = "";
//		if (typeOperation.equals("R")) {
//			op = "Retrait ";
//		}
//		if (typeOperation.equals("A")) {
//			op = "Credit  ";
//		}
//		if (typeOperation.equals("V")) {
//			op = "Virement";
//		}
//		return op;
//	}

//	/**
//	 * Fonction qui retourne le type de compte
//	 * 
//	 * @param compte code du type de compte
//	 * @return type de compte
//	 */
//	public String typeCompte(String compte) {
//		String LibCompte = "";
//		if (compte.equals("Compte")) {
//			LibCompte = "Compte courant";
//		}
//		if (compte.equals("livretA")) {
//			LibCompte = "Livret A      ";
//		}
//		if (compte.equals("PEL")) {
//			LibCompte = "P.E.L.        ";
//		}
//		return LibCompte;
//	}
}
