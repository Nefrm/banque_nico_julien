package cda.banque.services;

import java.time.LocalDate;
import java.util.Scanner;

import cda.banque.control.ControlsClient;
import cda.banque.control.ControlsGeneral;
import cda.banque.entite.Administrateur;
import cda.banque.entite.Client;
import cda.banque.entite.Compte;
import cda.banque.entite.Conseille;

public class ServiceConseille {
	//Attributs
	private static int logIncrementConseille = -1;
	
	//M�thodes
	/**
	 * Cr�er un conseill�
	 * 
	 * @return	un nouveau conseill�
	 */
	public Conseille creerConseille(Scanner sc) {
		String log = "CO";
		if(logIncrementConseille >= 10000) {
			System.out.println("le nombre de conseill� maximum a �t� atteint");
			return null;
		}else {
			String str = "";
			str += ++logIncrementConseille;
			while(str.length() < 4)
				str = "0"+str;
			log += str;
		}
		String nom = null;
		String prenom = null;
		LocalDate dateNaissance = LocalDate.now().plusDays(1);
		String date = null;
		String mail = null;
		do {
			System.out.println("veuillez saisir un nom :");
			nom = sc.nextLine();
		}while(!ControlsClient.validNomPrenom(nom));
		do {
			System.out.println("veuillez saisir un pr�nom :");
			prenom = sc.nextLine();
		}while(!ControlsClient.validNomPrenom(prenom));
		while(dateNaissance.isAfter(LocalDate.now())) {
			System.out.println("veuillez saisir une date de naissance (dd/mm/yyyy):");
			date = sc.nextLine();
			if(ControlsGeneral.validDate(date))
				dateNaissance = LocalDate.of(Integer.parseInt(date.substring(6)), Integer.parseInt(date.substring(3,5)), Integer.parseInt(date.substring(0,2)));
		}
		do {
			System.out.println("veuillez saisir une adresse mail :");
			mail = sc.nextLine();
		}while(!ControlsGeneral.validEMail(mail));
		
		return new Conseille(nom, prenom, dateNaissance, mail, log);
	}

	/**
	 * Recherche un compte via le conseille
	 * 
	 * @param conseille	le conseille
	 * @param compte	le compte rechercher
	 * @return	le compte s'il existe, null sinon
	 */
	public static Compte rechercheCompte(Conseille conseille, String numCompte) {
		for (Client client : conseille.getClient()) {
			Compte compte = ServicesClient.rechercheCompte(client, numCompte);
			if(compte != null)
				return compte;
		}
		return null;
	}
	/**
	 * Rechercher un client via le conseille
	 * @param conseille le conseille
	 * @param login le login de client recherche
	 * @return le client s'il existe null sinon 
	 */
	public static Client rechercheClient(Conseille conseille, String numClient) {
		for (Client client : conseille.getClient()) {
			if(client != null && client.getNumClient().equals(numClient))
				return client;
		}
		return null;
	}

//	/**
//	 * alimentation
//	 * 
//	 * @param conseille	le conseille
//	 * @param numCompte	le numero du compte
//	 * @param montant	le scanner
//	 * @param admin 
//	 * @return	true si l'alimentation a ete effectuer, false si non
//	 */
//	public static boolean alimenterUnCompte(Administrateur admin, Conseille conseille, String numCompte, double montant) {
//		for (Client client : conseille.getClient()) {
//			return ServicesClient.alimenterUnCompte(admin, client, numCompte, montant);
//		}
//		return false;
//	}
	
//	/**
//	 * Retire de l'argent
//	 * 
//	 * @param conseille	le conseille
//	 * @param numCompte	le numero du compte
//	 * @param montant	le scanner
//	 * @return	true si l'argent est retirer, false si non
//	 */
//	public static boolean retirerArgent(Administrateur admin, Conseille conseille, String numCompte, double montant) {
//		for (Client client : conseille.getClient()) {
//			if(ServicesClient.retirerArgent(admin, client, numCompte, montant))
//				return true;
//		}
//		return false;
//	}
}
