package cda.banque.services;

import java.util.Scanner;

import cda.banque.entite.LivretA;

public class ServiceLivretA {
	//M�thode(s)
	/**
	 * Cr�er un nouveau compte de type livretA
	 * 
	 * @return	un compte LivretA
	 */
	public LivretA creerLivretA(Scanner sc) {
		char reponse = ' ';
		while(reponse != 'o' && reponse != 'n') {
			System.out.println("autoriser le d�couvert ? o/n");
			reponse = sc.nextLine().toLowerCase().charAt(0);
		}
		if(reponse == 'o')
			return new LivretA(true);
		else
			return new LivretA(false);
	}
}
