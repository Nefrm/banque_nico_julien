package cda.banque.ihm;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import cda.banque.services.ServiceAdministrateur;
import cda.banque.services.Services;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class IhmClient {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IhmClient window = new IhmClient();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public IhmClient() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.SOUTH);
		
		JButton btnDconnection = new JButton("Déconnection");
		btnDconnection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IhmConnection.main(null);
				frame.setVisible(false);
			}
		});
		panel.add(btnDconnection);
		
		JButton btnValider = new JButton("Valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (1) {
//				case "1": // Consulter les infos d'un client
//					consulterInfoClient(client);
//					break;
//				case "2": // Consulter les infos d'un client et les information de ses comptes
//					consulterInfoClient(client);
//					consulterCompteClient(client);
//					break;
//				case "3": 
//					//consulterOperation(client , compte);
//					new Services().consulterOperation(administrateur, client, in);
//					break;
//				case "4": 	// Virement
//					IhmClient1.consulterCompteClient(client);
//							System.out.println("Entrer le numero de votre compte a debiter");			
//							String comptDeb = in.nextLine();
//							System.out.println("Entrer le numero du compte a crediter");
//								 String comptCred= in.nextLine();
//					ServiceAdministrateur.virement(administrateur, comptDeb, comptCred, in);
//					break;
//				case "5":	//imprimerReleveCompte(client , compte); + date
//					break;
//				case "6":	// Alimenter un compte
//					System.out.println("Entrer le numero de compte a crediter");
//					String numCompte = in.nextLine();
//					System.out.println("Quel montant voulez-vous ajouter ?");
//					double montant = in.nextDouble();
//					in.nextLine();
//					ServiceAdministrateur.alimenterUnCompte(administrateur ,  numCompte, montant);
//					break;
//				case "7": // Retait
//					System.out.println("Entrer le numero de compte a debiter");
//					String numCompteDeb = in.nextLine();
//					System.out.println("Quel montant voulez-vous retirer ?");
//					double montant2 = in.nextDouble();
//					in.nextLine();
//					ServiceAdministrateur.retirerArgent(administrateur , numCompteDeb, montant2);
//					break;
//				case "8": quitter = true;
//					
				default:
					break;
				}
			}
		});
		panel.add(btnValider);
		
		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, BorderLayout.WEST);
		
		JList list = new JList();
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"Consulter mes informations", "Consulter mes comptes", "Consulter mes opérations", "Virement", "Imprimer un relevé d'un compte", "Alimenter un compte", "Retait", "Quitter"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		panel_1.add(list);
		
		JPanel panel_2 = new JPanel();
		frame.getContentPane().add(panel_2, BorderLayout.NORTH);
		
		JLabel lblClient = new JLabel("Client");
		panel_2.add(lblClient);
	}

}
