package cda.banque.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cda.banque.services.Services;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.BevelBorder;

public class IhmCreationClient {

	private JFrame frmCrationDunClient;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JLabel lblNewLabel;
	private JComboBox<String> comboBox;
	private JTextField textField_3;
	private JComboBox comboBox_1;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IhmCreationClient window = new IhmCreationClient();
					window.frmCrationDunClient.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public IhmCreationClient() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCrationDunClient = new JFrame();
		frmCrationDunClient.setTitle("Création d'un client");
		frmCrationDunClient.setBounds(100, 100, 300, 350);
		
		JPanel panel = new JPanel();
		frmCrationDunClient.getContentPane().add(panel, BorderLayout.SOUTH);
		
		JButton btnRetour = new JButton("Retour");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmCrationDunClient.setVisible(false);
			}
		});
		panel.add(btnRetour);
		
		JButton btnValider = new JButton("Valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(Services.ajoutClientBDD(textField.getText(), textField_1.getText(), textField_2.getText(), (String) comboBox.getSelectedItem(), (String) comboBox_1.getSelectedItem(), textField_3.getText(), table))
					frmCrationDunClient.setVisible(false);
				else
					lblNewLabel.setText("information incorrect");
			}
		});
		panel.add(btnValider);
		
		JPanel panel_1 = new JPanel();
		frmCrationDunClient.getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);
		
		JLabel lblNom = new JLabel("Nom");
		lblNom.setBounds(10, 11, 46, 14);
		panel_1.add(lblNom);
		
		JLabel lblPrenom = new JLabel("Prenom");
		lblPrenom.setBounds(10, 36, 46, 14);
		panel_1.add(lblPrenom);
		
		JLabel lblDateDeNaissance = new JLabel("Date de naissance");
		lblDateDeNaissance.setBounds(10, 61, 100, 14);
		panel_1.add(lblDateDeNaissance);
		
		textField = new JTextField();
		textField.setBounds(117, 8, 130, 20);
		panel_1.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(117, 33, 130, 20);
		panel_1.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(117, 58, 130, 20);
		panel_1.add(textField_2);
		textField_2.setColumns(10);
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setBounds(10, 253, 264, 14);
		panel_1.add(lblNewLabel);
		
		JLabel lblAgence = new JLabel("Agence");
		lblAgence.setBounds(10, 86, 46, 14);
		panel_1.add(lblAgence);
		
		JLabel lblConseille = new JLabel("Conseille");
		lblConseille.setBounds(10, 111, 100, 14);
		panel_1.add(lblConseille);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(Services.getAgence()));
		comboBox.setBounds(117, 83, 130, 20);
		panel_1.add(comboBox);
		
		comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(Services.getConseille()));
		comboBox_1.setBounds(117, 108, 130, 20);
		panel_1.add(comboBox_1);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 136, 46, 14);
		panel_1.add(lblEmail);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(117, 133, 130, 20);
		panel_1.add(textField_3);
		
		JButton btnAjouterUnCompte = new JButton("Ajouter un compte");
		btnAjouterUnCompte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IhmCreationCompte.main(null);
			}
		});
		btnAjouterUnCompte.setBounds(10, 161, 160, 23);
		panel_1.add(btnAjouterUnCompte);
		
		table = new JTable();
		table.setRowSelectionAllowed(false);
		table.setEnabled(false);
		table.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"Numero de compte", "Type", "Solde", "Decouvert"
			}
		));
		table.getColumnModel().getColumn(0).setPreferredWidth(110);
		table.setBounds(10, 199, 264, 48);
		panel_1.add(table);
	}
}
