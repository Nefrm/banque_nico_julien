package cda.banque.ihm;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;

import cda.banque.services.Services;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.Color;

public class IhmConnection {

	private static JFrame frmConnection;
	private JTextField textFieldLogin;
	private JPasswordField passwordField;
	private JLabel labelIncorrect;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IhmConnection window = new IhmConnection();
					window.frmConnection.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public IhmConnection() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmConnection = new JFrame();
		frmConnection.setTitle("Connection");
		frmConnection.setBounds(100, 100, 350, 200);
		frmConnection.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frmConnection.getContentPane().add(panel, BorderLayout.SOUTH);
		
		JButton btnQuitter = new JButton("Quitter");
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		panel.add(btnQuitter);
		
		JButton btnConnection = new JButton("Connection");
		btnConnection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!Services.login(textFieldLogin.getText(), passwordField.getText()))
					labelIncorrect.setText("Login ou Mot de Passe Incorrect");
				
			}
		});
		panel.add(btnConnection);
		
		JPanel panel_1 = new JPanel();
		frmConnection.getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Login");
		lblNewLabel.setBounds(41, 26, 83, 32);
		panel_1.add(lblNewLabel);
		
		JLabel lblMotDePasse = new JLabel("Mot de passe");
		lblMotDePasse.setBounds(41, 59, 83, 32);
		panel_1.add(lblMotDePasse);
		
		textFieldLogin = new JTextField();
		textFieldLogin.setBounds(134, 32, 140, 20);
		panel_1.add(textFieldLogin);
		textFieldLogin.setColumns(10);
		
		labelIncorrect = new JLabel("");
		labelIncorrect.setForeground(Color.RED);
		labelIncorrect.setBounds(10, 102, 314, 14);
		panel_1.add(labelIncorrect);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(134, 65, 140, 20);
		panel_1.add(passwordField);
	}
	
	public static void deco() {
		frmConnection.setVisible(false);
	}
}
