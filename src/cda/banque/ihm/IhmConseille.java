package cda.banque.ihm;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import cda.banque.entite.Agence;
import cda.banque.entite.Client;
import cda.banque.entite.Conseille;
import cda.banque.services.ServiceAdministrateur;
import cda.banque.services.ServiceAgence;
import cda.banque.services.ServiceCompte;
import cda.banque.services.ServiceConseille;
import cda.banque.services.Services;
import cda.banque.services.ServicesClient;

import java.awt.BorderLayout;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.AbstractListModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IhmConseille {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IhmConseille window = new IhmConseille();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public IhmConseille() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		
		JLabel lblConseille = new JLabel("Conseille");
		panel.add(lblConseille);
		
		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, BorderLayout.WEST);
		
		final JList list = new JList();
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"Créer un client", "Créer un compte", "Consulter les informations d'un client", "Consulter les comptes d'un client", "Consulter les opérations d'un client", "Virement", "Imprimer un relevé d'un compte", " Alimenter un compte", "Retait", "Modifier les informations d'un client", "Changer la domiciliation d'un client", "Quitter"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		panel_1.add(list);
		
		JPanel panel_2 = new JPanel();
		frame.getContentPane().add(panel_2, BorderLayout.SOUTH);
		
		JButton btnDconnection = new JButton("Déconnection");
		btnDconnection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IhmConnection.main(null);
				frame.setVisible(false);
			}
		});
		panel_2.add(btnDconnection);
		
		JButton btnValider = new JButton("Valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (list.getSelectedIndex()) {
				case 0:{  	// Creer un client
					IhmCreationClient.main(null);
					break;
				}
//				case "2":{ 	// Cree un compte
//					System.out.println("Entrer le numero du client : ");
//						Client client = ServiceConseille.rechercheClient(conseille, in.nextLine());
//							client.getCompte().add(new ServiceCompte().choixCompte(client, in));
//					break;
//				}
//				case "3":{ 	// Consulter les information d'un client
//					System.out.println("Entrer le numero du client : ");
//					IhmClient1.consulterInfoClient(ServiceConseille.rechercheClient(conseille , in.nextLine()));
//					break;
//				}
//				case "4":{	// Consulter les comptes d'un client
//					System.out.println("Entrer le numero du client : ");
//					IhmClient1.consulterCompteClient(ServiceConseille.rechercheClient(conseille , in.nextLine()));
//					break;
//				}
//				case "5":{	// Consulter les op�rations d'un client
//					System.out.println("Entrer le numero du client : ");
//					String numClient=in.nextLine();
//					Client client =ServiceConseille.rechercheClient(conseille, numClient);
//					new Services().consulterOperation(administrateur, client, in);
//					break;
//				}
//				case "6":{	// Virement entre 2 comptes
//					System.out.println("Entrer le numero de compte a debiter :");
//					System.out.println("Entrer le numero de compte a crediter");
//					ServiceAdministrateur.virement(administrateur, in.nextLine(), in.nextLine(), in);
//					break;
//				}
//				case "7":{	// Imprimer un relev� d�un compte
//					break;
//				}
//				case "8":{ 	// Alimenter un compte
//					System.out.println("Entrer le numero de compte du client : ");
//					String numCompte = in.nextLine();
//					System.out.println("Quel montant voulez-vous ajouter ?");
//					double montant = in.nextDouble();
//					in.nextLine();
//					ServiceAdministrateur.alimenterUnCompte(administrateur, numCompte, montant);
//					break;
//				}
//				case "9":{ 	// Retait
//					System.out.println("Entrer le numero de compte du client : ");
//					String numCompte = in.nextLine();
//					System.out.println("Quel montant voulez-vous retirer ?");
//					double montant = in.nextDouble();
//					in.nextLine();
//					ServiceConseille.retirerArgent( administrateur,conseille , numCompte, montant);
//					break;
//				}
//				case "10":{ 	// Modifier les informations d�un client
//					System.out.println("Entrer le numero de compte du client : ");	
//					break;
//				}
//				case "11":{	// Changer la domiciliation d�un client
//					System.out.println("Entrer le numero de l'agence de domiciliation : ");
//					Agence agence1 = ServiceAdministrateur.rechercheAgence(administrateur, in.nextLine());
//				System.out.println("Entrer le numero du conseille : ");
//					Conseille conseille1 = ServiceAdministrateur.rechercheConseille(administrateur, in.nextLine());
//				System.out.println("Entrer le numero du client : ");
//					Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
//				System.out.println("Entrer le numero de l'agence de destinaion : ");
//					Agence agence2 = ServiceAdministrateur.rechercheAgence(administrateur, in.nextLine());
//				System.out.println("Entrer le numero de l'agence de destination : ");
//					Conseille conseille2 = ServiceAdministrateur.rechercheConseille(administrateur, in.nextLine());
//				new ServiceAgence().changerDomiciliation(agence1, conseille1, client, agence2, conseille2);
//					break;
//					}	
//				case "12": 	// Quitter
//					quitter = true;
//					
				default:
					break;
				}
			}
		});
		panel_2.add(btnValider);
	}

}
