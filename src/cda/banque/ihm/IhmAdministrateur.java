package cda.banque.ihm;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JList;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.UIManager;

import cda.banque.entite.Agence;
import cda.banque.entite.Client;
import cda.banque.entite.Conseille;
import cda.banque.services.ServiceAdministrateur;
import cda.banque.services.ServiceAgence;
import cda.banque.services.ServiceCompte;
import cda.banque.services.ServiceConseille;
import cda.banque.services.Services;
import cda.banque.services.ServicesClient;

import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IhmAdministrateur {

	private JFrame Admin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IhmAdministrateur window = new IhmAdministrateur();
					window.Admin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public IhmAdministrateur() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Admin = new JFrame();
		Admin.setTitle("Administrateur");
		Admin.setBounds(100, 100, 400, 400);
		Admin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		Admin.getContentPane().add(panel, BorderLayout.WEST);

		final JList list = new JList();
		panel.add(list);
		list.setVisibleRowCount(20);
		list.setValueIsAdjusting(true);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		list.setModel(new AbstractListModel() {
			String[] values = new String[] { "Créer une agence", "Créer un client", "Changer Domiciliation d'un client",
					"Modifier les informations d'un client", "Désactiver un client", "Activer un client",
					"Rechercher un client", "Créer un compte bancaire", "Désactiver un compte", "Activer un compte",
					"Rechercher un compte", "Afficher la liste des comptes", "Imprimer les infos clients",
					"Quitter le programme" };

			public int getSize() {
				return values.length;
			}

			public Object getElementAt(int index) {
				return values[index];
			}
		});

		JPanel panel_1 = new JPanel();
		Admin.getContentPane().add(panel_1, BorderLayout.NORTH);

		JLabel lblAdministrateur = new JLabel("Administrateur");
		panel_1.add(lblAdministrateur);

		JPanel panel_2 = new JPanel();
		Admin.getContentPane().add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnDconnection = new JButton("Déconnection");
		btnDconnection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IhmConnection.main(null);
				Admin.setVisible(false);
			}
		});
		panel_2.add(btnDconnection);

		JButton btnValider = new JButton("Valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (list.getSelectedIndex()) {
//				case 1: { // Cree une agence
//					administrateur.getAgence().add(new ServiceAgence().creerAgence(in));
//					Services.initRessources(administrateur);
//					break;
//				}
//				case 2: { // Cree un conseille
//					System.out.println("Entrer le code agence");
//					ServiceAdministrateur.rechercheAgence(administrateur, in.nextLine()).getConseille()
//							.add(new ServiceConseille().creerConseille(in));
//					Services.initRessources(administrateur);
//					break;
//				}
				case 1: { // Cree client
					IhmCreationClient.main(null);
					break;
				}
//				case 4: { // Cree un compte
//					System.out.println("Entrer le numero du client : ");
//					Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
//					client.getCompte().add(new ServiceCompte().choixCompte(client, in));
//					Services.initRessources(administrateur);
//					break;
//				}
//				case 5: { // Consulter les informations d'un client
//					System.out.println();
//					System.out.println("Entrer le numero du client : ");
//					IhmClient.consulterInfoClient(ServiceAdministrateur.rechercheClient(administrateur, in.nextLine()));
//					break;
//				}
//				case 6: { // Consulter les comptes d'un client
//					System.out.println("Entrer le numero du client : ");
//					IhmClient.consulterCompteClient(
//							ServiceAdministrateur.rechercheClient(administrateur, in.nextLine()));
//					break;
//				}
//				case 7: { // Consulter les operations d'un client
//					System.out.println("Entrer le numero du client : ");
//					String numClient = in.nextLine();
//					Client client = ServiceAdministrateur.rechercheClient(administrateur, numClient);
//					new Services().consulterOperation(administrateur, client, in);
//					break;
//				}
//				case 8: { // Virement entre 2 comptes
//					System.out.println("Entrer le numero de compte a debiter :");
//					System.out.println("Entrer le numero de compte a crediter");
//					ServiceAdministrateur.virement(administrateur, in.nextLine(), in.nextLine(), in);
//					break;
//				}
//				case 9: { // imprimer un releve de compte
//
//					break;
//				}
//				case 10: { // Alimenter un compte
//					System.out.println("Entrer le numero de compte du client : ");
//					String numCompte = in.nextLine();
//					System.out.println("Quel montant voulez-vous ajouter ?");
//					double montant = in.nextDouble();
//					in.nextLine();
//					ServiceAdministrateur.alimenterUnCompte(administrateur, numCompte, montant);
//					break;
//				}
//				case 11: { // Retrait
//					System.out.println("Entrer le numero de compte du client : ");
//					String numCompte = in.nextLine();
//					System.out.println("Quel montant voulez-vous retirer ?");
//					double montant = in.nextDouble();
//					in.nextLine();
//					ServiceAdministrateur.retirerArgent(administrateur, numCompte, montant);
//					break;
//				}
//				case 12: { // Modifier les informations
//					System.out.println("Entrer le numero du client : ");
//					ServicesClient.modifInfoClient(ServiceAdministrateur.rechercheClient(administrateur, in.nextLine()),
//							in);
//					break;
//				}
//				case 13: { // Changer la domiciliation d'un client
//					System.out.println("Entrer le numero de l'agence de domiciliation : ");
//					Agence agence1 = ServiceAdministrateur.rechercheAgence(administrateur, in.nextLine());
//					System.out.println("Entrer le numero du conseille : ");
//					Conseille conseille1 = ServiceAdministrateur.rechercheConseille(administrateur, in.nextLine());
//					System.out.println("Entrer le numero du client : ");
//					Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
//					System.out.println("Entrer le numero de l'agence de destinaion : ");
//					Agence agence2 = ServiceAdministrateur.rechercheAgence(administrateur, in.nextLine());
//					System.out.println("Entrer le numero de l'agence de destination : ");
//					Conseille conseille2 = ServiceAdministrateur.rechercheConseille(administrateur, in.nextLine());
//					new ServiceAgence().changerDomiciliation(agence1, conseille1, client, agence2, conseille2);
//					break;
//				}
//				case 14: { // Activer un compte
//					System.out.println("Entrer le numero du client : ");
//					Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
//					System.out.println("Entrer le numero de compte que vous voulez activer : ");
//					new ServiceCompte().activerUnCompte(client, in.nextLine());
//					break;
//				}
//				case 15: { // Desactiver un compte
//					System.out.println("Entrer le numero du client : ");
//					Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
//					System.out.println("Entrer le numero de compte que vous voulez desactiver : ");
//					new ServiceCompte().desactiverUnCompte(client, in.nextLine());
//					break;
//				}
//				case 16: { // Activer un client
//					System.out.println("Entrer le numero du client : ");
//					Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
//					ServicesClient.activerClient(client);
//					break;
//				}
//				case 17: { // Desactiver un client
//					System.out.println("Entrer le numero du client : ");
//					Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
//					ServicesClient.desactiverClient(client);
//					break;
//				}
//				case 18: { // Quitter
//					quitter = true;
//					break;
//				}
				default: {
					break;
				}
				}

			}
		});
		panel_2.add(btnValider);
	}

}
