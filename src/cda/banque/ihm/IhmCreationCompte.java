package cda.banque.ihm;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IhmCreationCompte {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IhmCreationCompte window = new IhmCreationCompte();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public IhmCreationCompte() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 280, 160);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.SOUTH);
		
		JButton btnRetour = new JButton("Retour");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		panel.add(btnRetour);
		
		JButton btnValider = new JButton("Valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		panel.add(btnValider);
		
		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(null);
		
		JLabel lblTypeDeCompte = new JLabel("type de compte");
		lblTypeDeCompte.setBounds(10, 11, 102, 14);
		panel_1.add(lblTypeDeCompte);
		
		JLabel lblSoldeDeDpart = new JLabel("Solde de départ");
		lblSoldeDeDpart.setBounds(10, 36, 102, 14);
		panel_1.add(lblSoldeDeDpart);
		
		JRadioButton rdbtnDcouvertAutoris = new JRadioButton("Découvert autorisé");
		rdbtnDcouvertAutoris.setBounds(6, 57, 140, 23);
		panel_1.add(rdbtnDcouvertAutoris);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Courant", "LivretA", "PEL"}));
		comboBox.setBounds(122, 8, 100, 20);
		panel_1.add(comboBox);
		
		textField = new JTextField();
		textField.setBounds(122, 33, 100, 20);
		panel_1.add(textField);
		textField.setColumns(10);
	}
}
