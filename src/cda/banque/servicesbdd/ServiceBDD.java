package cda.banque.servicesbdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Properties;

import javax.swing.JTable;

import cda.banque.ihm.IhmAdministrateur;
import cda.banque.ihm.IhmClient;
import cda.banque.ihm.IhmConnection;
import cda.banque.ihm.IhmConseille;

public class ServiceBDD {
	static String url = "jdbc:postgresql://localhost:5432/banque";
	static Properties props = new Properties();
	static Connection conn;
	static Statement stmt;
	static boolean init = false;

	public static void initialisation() {
		if (!init) {
			// Charger et configurer le driver de la base de données.
			String driverName = "org.postgresql.Driver";
			try {
				Class.forName(driverName);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Infos de connection
			props.setProperty("user", "postgres");
			props.setProperty("password", "admin");

			try {
				conn = DriverManager.getConnection(url, props);
				stmt = conn.createStatement();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			init = true;
		}
	}

	public static void quit() {
		if (init) {
			try {
				conn.close();
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			init = false;
		}
	}

	public static boolean login(String login, String mdp) throws SQLException {
		initialisation();
		ResultSet resultats = null;
		// Requette à executer
		String requete = "SELECT * FROM personne WHERE login = \'" + login + "\';";
		try {
			resultats = stmt.executeQuery(requete);
			while (resultats.next()) {
				if (mdp.equalsIgnoreCase(resultats.getString("mdp"))) {
					if (resultats.getInt("id_type_personne") == 1) { // admin
						IhmConnection.deco();
						IhmAdministrateur.main(null);
						return true;
					} else if (resultats.getInt("id_type_personne") == 2) { // conseiller
						System.out.println("conseiller");
						IhmConnection.deco();
						IhmConseille.main(null);
						return true;

					} else { // client
						System.out.println("client");
						IhmConnection.deco();
						IhmClient.main(null);
						return true;

					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean ajoutClientBDD(String nom, String prenom, String naissance, String agence, String conseille,
			String email, JTable table) {
		initialisation();

		int id = getIndicePersonne();
		int log = getLogPersonne();
		int idA = getIdAgence(agence);
		int idC = getIdConseille(conseille);

		int i = 0;
		while (table.getValueAt(i, 0) != null) {
			i++;
		}
		String requete = "";
		
//		for(int j = 0; j<i; j++) 
			requete = "INSERT INTO personne VALUES (" + id + ",3,\'" + nom + "\',\'" + prenom + "\',\'" + naissance+ "\'," + idA + ", 0"+/*table.getValueAt(j, 0)+*/", \'" + log + "\', \'" + log + "\', true, " + idC + ", \'" + email + "\')";
		try {
			stmt.executeUpdate(requete);
		} catch (SQLException e) {
			return false;
		}

		return true;
	}

	private static int getIdConseille(String conseille) {
		initialisation();
		String requete = "SELECT id_personne FROM personne WHERE nom LIKE \'" + conseille + "\';";
		ResultSet resultats = null;
		try {
			resultats = stmt.executeQuery(requete);
			while (resultats.next()) {
				return resultats.getInt("id_personne");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
		return -1;
	}

	private static int getLogPersonne() {
		initialisation();
		String requete = "SELECT login FROM personne WHERE id_type_personne = 3;";
		ResultSet resultats = null;
		int i = 1;
		try {
			resultats = stmt.executeQuery(requete);
			while (resultats.next()) {
				i++;
			}
			return i;
		} catch (SQLException e) {
			e.printStackTrace();
			return i;
		}
	}

	private static int getIdAgence(String agence) {
		initialisation();
		String requete = "SELECT id_agence FROM agence WHERE nom_agence LIKE \'" + agence + "\';";
		ResultSet resultats = null;
		try {
			resultats = stmt.executeQuery(requete);
			while (resultats.next()) {
				return resultats.getInt("id_agence");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
		return -1;
	}

	private static int getIndicePersonne() {
		initialisation();
		String requete = "SELECT id_personne FROM personne;";
		ResultSet resultats = null;
		int i = 1;
		try {
			resultats = stmt.executeQuery(requete);
			while (resultats.next()) {
				i++;
			}
			return i;
		} catch (SQLException e) {
			e.printStackTrace();
			return i;
		}
	}

	public static String[] getAgence() {
		initialisation();
		String requete = "SELECT nom_agence FROM agence;";
		ResultSet resultats = null;
		try {
			resultats = stmt.executeQuery(requete);
			ArrayList<String> res = new ArrayList<String>();

			while (resultats.next()) {
				res.add(resultats.getString("nom_agence"));
			}
			return res.toArray(new String[0]);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String[] getConseille() {
		initialisation();
		String requete = "SELECT nom FROM personne WHERE id_type_personne = 2;";
		ResultSet resultats = null;
		try {
			resultats = stmt.executeQuery(requete);
			ArrayList<String> res = new ArrayList<String>();

			while (resultats.next()) {
				res.add(resultats.getString("nom"));
			}
			return res.toArray(new String[0]);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
