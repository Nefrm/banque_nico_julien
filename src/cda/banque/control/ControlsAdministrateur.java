package cda.banque.control;

public class ControlsAdministrateur {
	/**
	 * Verification si le login correspond a ADM00
	 * 
	 * @param log	le login a verifier
	 * @return	true si le login est correct, false si non
	 */
	public static boolean validerLogin(String log) {
		if(log.matches("ADM\\d{2}"))
			return true;
		System.out.println("login admin incorrect");
		return false;
	}
	
	

}
