package cda.banque.control;

import java.util.ArrayList;

public class ControlsCompte {
	//Attribut
	private static ArrayList<String> numeroCompte = new ArrayList<String>();
	
	/**
	 * Valide le numero de compte
	 * 
	 * @param numero	le numero de compte
	 * @return	true si le numero est correct, false si non
	 */
	public static boolean validerNumeroCompte(String numero) {
		if(numero.matches("\\d{11}") && !numeroCompte.contains(numero)) {
			numeroCompte.add(numero);
			return true;
		}
		return false;
	}
}
