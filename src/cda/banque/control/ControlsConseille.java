package cda.banque.control;

public class ControlsConseille {
	/**
	 * valide le login du conseille
	 * 
	 * @param log	le log a verifier
	 * @return	true si le log est correct, false si non
	 */
	public static boolean validerLogin(String log) {
		if(log.matches("CO\\d{2}"))
			return true;
		System.out.println("login conseill� incorrect");
		return false;
	}
}
