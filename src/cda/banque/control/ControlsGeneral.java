package cda.banque.control;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;

import cda.banque.entite.Agence;
import cda.banque.services.Services;

public class ControlsGeneral {

	/**
	 * Fonction qui permet de verifier si la date est valide
	 * @param date : la date saisie
	 * @return un boolean pour confirmer que la saisie est correct
	 */
	public static boolean validDate(String date) {
		if (date != null && date.matches(
				"(0[1-9]|[12]\\d|3[01])/(0[1-9]|1[0-2])/([1-9][0-9]{3}|0[1-9][0-9]{2}|00[1-9][0-9]{1}|000[1-9])")) {
			return true;
		}
		return false;
	}
	/**
	 *  Fonction qui permet de verifier si l'email est valide
	 * @param eMail	:	l'adresse email saisie
	 * @return	:	un boolean pour confirmer que la saisie st correct
	 */
	public static boolean validEMail(String eMail) {
		if (eMail != null && eMail.matches("[\\w][\\w\\-]*(\\.[\\w\\-]+)*+@[a-zA-Z0-9]{3,13}\\.[a-zA-Z]{2,3}")) {
			return true;
		}
		return false;
	}
	
	
	/**
	 * Verifie si les login et mot de passe correspondent a un client de l'agence
	 * 
	 * @param agence	l'agence cible
	 * @param login		le login du client
	 * @param motDePasse	le mot de passe du client
	 * @return	true si les logs sont correct, false si non
	 * @throws IOException
	 */
	public static boolean verificationLoginClient(Agence agence, String login, String motDePasse) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("ressource\\"+agence.getCodeAgence()+"\\clients\\listeClients.txt"));
		while(br.ready()) {
			String[] log = br.readLine().split("~");
			if(log[0].equals(login) && Services.decodage(log[1]).equals(motDePasse)) {
				br.close();
				return true;
			}
		}
		br.close();
		return false;
	}
	
	/**
	 * Verifie si les login et mot de passe correspondent a un conseille de l'agence
	 * 
	 * @param agence	l'agence cible
	 * @param login		le login du conseille
	 * @param motDePasse	le mot de passe du conseille
	 * @return	true si les logs sont correct, false si non
	 * @throws IOException
	 */
	public static boolean verificationLoginConseille(Agence agence, String login, String motDePasse) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("ressource\\"+agence.getCodeAgence()+"\\conseille\\listeConseille.txt"));
		while(br.ready()) {
			String[] log = br.readLine().split("~");
			if(log[0].equals(login) && Services.decodage(log[1]).equals(motDePasse)) {
				br.close();
				return true;
			}
		}
		br.close();
		return false;
	}
	
	/**
	 * Verifie si les login et mot de passe correspondent a un admin
	 * 
	 * @param login		le login de l'admin
	 * @param motDePasse	le mot de passe de l'admin
	 * @return	true si les logs sont correct, false si non
	 * @throws IOException
	 */
	public static boolean verificationLoginAdmin(String login, String motDePasse) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("ressource\\admin.txt"));
		while(br.ready()) {
			String[] log = br.readLine().split("~");
			if(log[0].equals(login) && Services.decodage(log[1]).equals(motDePasse)) {
				br.close();
				return true;
			}
		}
		br.close();
		return false;
	}
	/**
	 * Fonction qui verifie que la date du debut de la ligne de transaction est bien comprise entre les 2 dates
	 * @param date1	la date du debut de la peroide
	 * @param date2	la date definde la periode
	 * @param transactLigne	la ligne de transaction
	 * @return	un boolean qui verifie si la date est bien dans l'interval
	 */
	public static boolean validTransacDate(String dateDebut , String dateFin , String transactLigne) {
		LocalDate dateL3 = Services.date2(transactLigne.substring(0,10));
		if (!dateL3.isBefore(Services.date(dateDebut)) && !dateL3.isAfter(Services.date(dateFin))){
			return true;
		}
		return false;
	}
}
