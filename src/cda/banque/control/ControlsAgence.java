package cda.banque.control;

public class ControlsAgence {
	
	/**
	 * verifie si l'addresse de l'agence est correct
	 * 
	 * @param addresseAgence	l'addresse de l'agence
	 * @return	true si le code est valide, false si non
	 */
	public static boolean validerAddresseAgence(String addresseAgence) {
		if (addresseAgence.matches("\\w{2,}"))
			return true;
		return false;
	}

	/**
	 * valide le nom de l'agence
	 * 
	 * @param nomA	le nom a verifier
	 * @return	true si le nom est valide, false si non
	 */
	public static boolean validNomAgence(String nomA) {
		if (nomA.matches("\\w{2,}"))
			return true;
		return false;
	}
	
}
