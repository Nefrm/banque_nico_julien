package cda.banque.control;

import java.util.ArrayList;

public class ControlsClient {

	public static ArrayList<String> listeNumCompte = new ArrayList<String>();

	/**
	 * Fonction qui verifie que le nom est bien une saisie alphabetique
	 * 
	 * @param nom la saisie demand�e
	 * @return un boolean pour confirmer que la saisie est correct
	 */
	public static boolean validNomPrenom(String nom) {

		if (nom != null && nom.matches("[A-Za-z]{2,}")) {
			return true;
		}
		System.out.println("Les donnees ne sont pas correct! Reessayer!");
		return false;
	}
	/**
	 * Fonction qui verifie le format du numero du client
	 * @param numClient le saisie  a verifier
	 * @return	un boolean pour verifier le format
	 */
	public static boolean validnumClient(String numClient) {
		if(numClient.matches("[A-Z]{2}[0-9]{6}")){
			return true;
		}
		return false;
	}

	/**
	 * Fonction qui verifie si le numero de compte existe deja
	 * @param num	le numero de compte genere de maniere aleatoir
	 * @return	un boolean pour verifier s'il faut regener un numeron
	 */
	public static boolean controleNumeroClient(String num) {
		if(!listeNumCompte.contains(num)) {
				listeNumCompte.add(num);
			return true;
			}
		return false;
	} 

}
