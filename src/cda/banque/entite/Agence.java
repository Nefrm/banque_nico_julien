package cda.banque.entite;

import java.util.ArrayList;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
public class Agence {
	private String codeAgence;
	private String nomAgence;
	private String adressAgence;
	private ArrayList<Conseille>conseille;
	private static int codeAgenceIncrement;
	
	public Agence(String nomAgence, String adressAgence) {
		String code = ""+(++codeAgenceIncrement);
		while (code.length() < 3)
			code = '0'+code;
		this.codeAgence = code;
		this.nomAgence = nomAgence;
		this.adressAgence = adressAgence;
		this.conseille = new ArrayList<Conseille>() ;
	}
}
