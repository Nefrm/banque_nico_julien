package cda.banque.entite;

import java.util.Random;

import cda.banque.control.ControlsCompte;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
public abstract class Compte {
	private String numero;
	private double solde;
	private boolean decouvert;
	private final double frais = 25;
	private boolean active;

	/**
	 * Constructeur du compte
	 * 
	 * @param decouvert	savoir si le compte peut etre a découvert ou non
	 */
	public Compte(boolean decouvert) {
		this.decouvert = decouvert;
		this.solde = 0.0;
		this.active = true;
		String num = "";
		do {
			num = "";
			for (int i = 0; i < 11; i++) {
				num += new Random().nextInt(10);
			}
		}while (!ControlsCompte.validerNumeroCompte(num));
		this.numero = num;
	}
}
