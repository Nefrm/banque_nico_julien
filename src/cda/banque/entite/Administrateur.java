package cda.banque.entite;

import java.time.LocalDate;
import java.util.ArrayList;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
public class Administrateur extends Conseille {
	private ArrayList<Agence>agence;
  
	public Administrateur(String nom, String prenom, LocalDate dateDeNaissance, String eMail,String login) {
		super(nom, prenom, dateDeNaissance, eMail, login);
		this.agence = new ArrayList<Agence>();
	}
}
