package cda.banque.entite;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
public class PEL extends Compte {
	/**
	 * Constructeur de la classe PEL
	 * 
	 * @param decouvert	le découvert autorisée ou non
	 */
	public PEL(boolean decouvert) {
		super(decouvert);
	}
}
