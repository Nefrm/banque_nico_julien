package cda.banque.entite;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
public class LivretA extends Compte {
	/**
	 * Constructeur de la classe LivretA
	 * 
	 * @param decouvert	le découvert autorisée ou non
	 */
	public LivretA(boolean decouvert) {
		super(decouvert);
	}
}
