package cda.banque.entite;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
public class Courant extends Compte {

	public Courant(boolean decouvert) {
		super(decouvert);
	}
}
