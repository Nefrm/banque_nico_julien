package cda.banque.entite;

import java.time.LocalDate;
import java.util.ArrayList;

import cda.banque.services.ServicesClient;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
public class Client extends Personne {
	private String numClient;
	private String login;
	private static int loginIncrement = 1;
	private boolean active;
	private ArrayList<Compte> compte;
	
	public Client(String nom, String prenom, LocalDate dateDeNaissance, String eMail) {
		super(nom, prenom, dateDeNaissance, eMail);
		numClient = ServicesClient.numClient(nom.charAt(0), prenom.charAt(0), loginIncrement);
		this.active = true;
		this.login = String.valueOf(loginIncrement++);
			while(this.login.length()<=10) {
				this.login="0"+this.login;
			}
		this.compte = new ArrayList<Compte>();		
	}
}
