package cda.banque.entite;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
public class Conseille extends Personne {

	private List<Client> client;
	private final String login;

	public Conseille(String nom, String prenom, LocalDate dateDeNaissance, String eMail, String login) {
		super(nom, prenom, dateDeNaissance, eMail);
		this.client = new LinkedList<Client>();
		this.login = login;
	}
}
