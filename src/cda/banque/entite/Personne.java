package cda.banque.entite;

import java.time.LocalDate;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@EqualsAndHashCode
public abstract class Personne {
	private String nom;
	private String prenom;
	private LocalDate dateDeNaissance;
	private String eMail;

	// Constructeur
	public Personne(String nom, String prenom, LocalDate dateDeNaissance, String eMail) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateDeNaissance = dateDeNaissance; // String + verif localdate
		this.eMail = eMail;
	}
}
