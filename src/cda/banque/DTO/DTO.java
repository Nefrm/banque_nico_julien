package cda.banque.DTO;

import java.sql.SQLException;

import javax.swing.JTable;

import cda.banque.servicesbdd.ServiceBDD;

public class DTO {

	public static boolean login(String login, String mdp) {
		try {
			return ServiceBDD.login(login, mdp);
		} catch (SQLException e) {}
		return false;
	}

	public static String[] getAgence() {
		return ServiceBDD.getAgence();
	}
	
	public static String[] getConseille() {
		return ServiceBDD.getConseille();
	}
	
	public static boolean ajoutClientBDD(String nom, String prenom, String naissance, String agence, String conseille, String email, JTable table) {
		return ServiceBDD.ajoutClientBDD(nom, prenom, naissance, agence, conseille, email, table);
	}
}
